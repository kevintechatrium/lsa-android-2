package tlive.techatrium.com.lsa.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.Calendar;
import java.util.HashMap;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import tlive.techatrium.com.lsa.Activities.ChangeProfileActivity;
import tlive.techatrium.com.lsa.Activities.MainActivity;
import tlive.techatrium.com.lsa.Models.User;
import tlive.techatrium.com.lsa.R;
import tlive.techatrium.com.lsa.Activities.SplashActivity;

/**
 * Created by hongt on 01/11/2017.
 */

@SuppressLint("ValidFragment")
public class ProfileFragment extends Fragment implements View.OnClickListener, MainActivity.CallbackActivity {

    private ImageView TakePhoto, Cover;
    private CircleImageView ProfilePhoto;
    private TextView Name, Wallet, Location, Email, Mobile, Bio, Gender, Age, Logout, Id, Fans, Follow;
    private TableRow btnName, btnWallet, btnLocation, btnMobile, btnBio, btnGender, btnAge, BtnPassword;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage;
    private FirebaseAuth auth;
    private User infoUser;
    private static String TAG = "ProfileDebug";
    private Context context;
    private boolean isCover;
    private ProgressDialog progDailog;
    private StorageReference storageRef;
    private StorageMetadata metadata;
    private LinearLayout loadingLayout;
    private TableLayout infoUserLayout;
    private TextView UpdateChange;
    private static ProfileFragment profileFragment;
    private View view;
    @SuppressLint("ValidFragment")
    public ProfileFragment(DatabaseReference mDatabase, FirebaseStorage storage, FirebaseAuth auth) {
        this.mDatabase = mDatabase;
        this.storage = storage;
        this.auth = auth;
    }
    public static ProfileFragment getInstance(DatabaseReference mDatabase, FirebaseStorage storage, FirebaseAuth auth) {
        profileFragment = profileFragment == null ? new ProfileFragment(mDatabase,storage,auth) : profileFragment;
        return profileFragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        ((MainActivity) getActivity()).setDataReceivedListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.profile_frament, container, false);
            initControl(view);
            ((MainActivity) getActivity()).setDataReceivedListener(this);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void showProgess(String title, String message) {
        progDailog = ProgressDialog.show(context,
                title,
                message, true);
    }

    private void initData() {
        context = getActivity();
        storageRef = storage.getReference();
        metadata = new StorageMetadata.Builder()
                .setContentType("image/jpg")
                .build();

        mDatabase.child("users").child(auth.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    infoUser = dataSnapshot.getValue(User.class);
                    initDisplay();
                    initEvent();
                } catch (Exception e) {
                    Log.e(TAG, "onDataChange: ", e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initControl(View view) {
        TakePhoto = (ImageView) view.findViewById(R.id.takePhoto);
        Cover = (ImageView) view.findViewById(R.id.userCover);

        ProfilePhoto = (CircleImageView) view.findViewById(R.id.profile_image);

        Name = (TextView) view.findViewById(R.id.txtName);
        Wallet = (TextView) view.findViewById(R.id.txtDiamond);
        Location = (TextView) view.findViewById(R.id.txtLocation);
        Email = (TextView) view.findViewById(R.id.txtEmail);
        Mobile = (TextView) view.findViewById(R.id.txtMobile);
        Bio = (TextView) view.findViewById(R.id.txtBio);
        Gender = (TextView) view.findViewById(R.id.txtGender);
        Age = (TextView) view.findViewById(R.id.txtAge);
        Logout = (TextView) view.findViewById(R.id.txtLogout);
        Id = (TextView) view.findViewById(R.id.txtId);
        Fans = (TextView) view.findViewById(R.id.txtFans);
        Follow = (TextView) view.findViewById(R.id.txtFollow);

        btnName = (TableRow) view.findViewById(R.id.btnName);
        btnWallet = (TableRow) view.findViewById(R.id.btnWallet);
        btnLocation = (TableRow) view.findViewById(R.id.btnLocation);
        btnMobile = (TableRow) view.findViewById(R.id.btnMobile);
        btnBio = (TableRow) view.findViewById(R.id.btnBio);
        btnGender = (TableRow) view.findViewById(R.id.btnGender);
        btnAge = (TableRow) view.findViewById(R.id.btnAge);
        BtnPassword = (TableRow) view.findViewById(R.id.BtnPassword);
        loadingLayout = (LinearLayout) view.findViewById(R.id.loading_content);
        infoUserLayout = (TableLayout) view.findViewById(R.id.infoUser);
    }

    private void initEvent() {
        btnName.setOnClickListener(this);
        btnWallet.setOnClickListener(this);
        btnLocation.setOnClickListener(this);
        btnMobile.setOnClickListener(this);
        btnBio.setOnClickListener(this);
        btnGender.setOnClickListener(this);
        btnAge.setOnClickListener(this);
        Logout.setOnClickListener(this);
        if(infoUser.getProvider().equalsIgnoreCase("email")){
            BtnPassword.setOnClickListener(this);
        }
        else {
            BtnPassword.setVisibility(View.GONE);
        }
        TakePhoto.setOnClickListener(this);
        ProfilePhoto.setOnClickListener(this);
    }

    private void initDisplay() {
        infoUserLayout.setVisibility(View.VISIBLE);
        loadingLayout.setVisibility(View.GONE);
        Name.setText(infoUser.getFullName());
        Wallet.setText((infoUser.getWallet().getDiamon_buy() - infoUser.getWallet().getDiamon_recieve()) + "");
        Location.setText(infoUser.getLocation());
        Email.setText(infoUser.getEmail());
        Mobile.setText(infoUser.getPhoneNumber());
        Bio.setText("Hey, I am a T-Live member");
        Gender.setText(infoUser.getGender());
        Id.setText("T-Live ID: " + infoUser.getT_LiveId());
        Fans.setText(infoUser.getFans() + "");
        Follow.setText(infoUser.getFollowing() + "");

        if (infoUser.getBirhtDay().length() > 0)
            Age.setText(getAge(infoUser.getBirhtDay()) + " (" + infoUser.getBirhtDay() + ")");
        if (infoUser.getAvatar().length() > 0)
            Picasso.with(context).load(infoUser.getAvatar()).into(ProfilePhoto);
        if (infoUser.getCoverImage().length() > 0)
            Picasso.with(context).load(infoUser.getCoverImage()).into(Cover);

    }

    private String getAge(String time) {

        int year = Integer.parseInt(time.substring(time.lastIndexOf("-") + 1));
        int month = Integer.parseInt(time.substring(time.indexOf("-") + 1, time.lastIndexOf("-")));
        int day = Integer.parseInt(time.substring(0, time.indexOf("-")));

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();
        return ageS;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnName:
                BtnChangeName();
                break;

            case R.id.btnWallet:
                BtnChangeWallet();
                break;

            case R.id.btnLocation:
                BtnChangeLocation();
                break;

            case R.id.btnMobile:
                BtnChangeMobile();
                break;

            case R.id.btnBio:
                BtnChangeBio();
                break;

            case R.id.btnGender:
                BtnChangeGender();
                break;

            case R.id.btnAge:
                BtnChangeAge();
                break;
            case R.id.BtnPassword:
                BtnChangePass();
                break;
            case R.id.txtLogout:
                BtnChangeLogout();
                break;
            case R.id.takePhoto:
                BtnTakeCover();
                break;

            case R.id.profile_image:
                BtnTakeAvatar();
                break;
        }
    }

    private void BtnTakeAvatar() {
        isCover = false;
        pickImage();
    }

    private void BtnTakeCover() {
        isCover = true;
        pickImage();
    }

    private void pickImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setAutoZoomEnabled(true)
                .setOutputCompressQuality(50)
                .start(getActivity());
    }

    private void BtnChangePass() {
        startIntent(R.integer.code_change_password, "Password", "");
    }

    private void BtnChangeName() {
        startIntent(R.integer.code_change_name, "Name", infoUser.getFullName());
    }

    private void BtnChangeWallet() {

    }

    private void BtnChangeLocation() {
        startIntent(R.integer.code_change_location, "Location", infoUser.getLocation());
    }

    private void BtnChangeEmail() {
        startIntent(R.integer.code_change_email, "Email", infoUser.getEmail());
    }

    private void BtnChangeMobile() {

        startIntent(R.integer.code_change_mobile, "Mobile", infoUser.getPhoneNumber());
    }

    private void BtnChangeBio() {

    }

    private void BtnChangeGender() {
        startIntent(R.integer.code_change_gender, "Gender", infoUser.getGender());
    }

    private void BtnChangeAge() {
        startIntent(R.integer.code_change_age, "Birth day", infoUser.getBirhtDay());
    }

    private void BtnChangeLogout() {
        FirebaseAuth.getInstance().signOut();
        context.startActivity(new Intent(context, SplashActivity.class));
        getActivity().finish();
    }

    private void startIntent(int type, String title, String value) {
        Intent intent = new Intent(context, ChangeProfileActivity.class);
        intent.putExtra("Type", type);
        intent.putExtra("Tile", title);
        intent.putExtra("Value", value);
        intent.putExtra("User", infoUser);
        getActivity().startActivityForResult(intent, 999);
    }

    @Override
    public void onResult(Intent data, int RequestCode, int ResultCode) {
        if (RequestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (ResultCode == Activity.RESULT_OK) {
                uploadPhoto(result);
            } else if (ResultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        if (RequestCode == 999 && ResultCode == Activity.RESULT_OK) {
            this.infoUser = (User) data.getSerializableExtra("User");
            initDisplay();
        }
    }

    private void uploadPhoto(final CropImage.ActivityResult result) {

        String nameFile;
        if (isCover) {
            nameFile = "cover_photos/" + createTransactionID();
        } else {
            nameFile = "avatar_photos/" + createTransactionID();
        }

        UploadTask uploadTask = storageRef.child(nameFile).putFile(result.getUri(), metadata);
        showProgess("Uploading", "Please wait...");


        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progDailog.cancel();
            }
        })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        updateProfilePhoto(taskSnapshot.getDownloadUrl() + "", result.getUri());
                    }
                });
    }

    private String createTransactionID() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }

    private void updateProfilePhoto(String url, final Uri image) {
        String child;
        if (isCover) child = "coverImage";
        else child = "avatar";

        mDatabase.child("users").child(auth.getUid()).child(child).setValue(url)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progDailog.cancel();
                        if (isCover) {
                            Cover.setImageURI(image);
                        } else {
                            ProfilePhoto.setImageURI(image);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "Update failure", Toast.LENGTH_SHORT).show();
                        progDailog.cancel();
                    }
                });
    }


}