package tlive.techatrium.com.lsa.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

import tlive.techatrium.com.lsa.Models.User;
import tlive.techatrium.com.lsa.R;

public class ChangeProfileActivity extends AppCompatActivity {
    private EditText inputEditText, pass;
    private String curentValue;
    private RadioButton male, female, other;
    private RadioGroup radioGroup;
    private TextInputLayout inputLayout, passLayout;
    private DatePicker birthDayPicker;
    private ProgressDialog progDailog;
    private int TypeChange;
    private User currentUser;
    private DatabaseReference mDatabase;
    private FirebaseUser user;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile);
        initData();
        initControl();
        initDisplay();
    }

    private void initDaypicker() {
        int year = 2017;
        int month = 0;
        int day = 1;
        if (curentValue.length() > 0) {
            year = Integer.parseInt(curentValue.substring(curentValue.lastIndexOf("-") + 1));
            month = Integer.parseInt(curentValue.substring(curentValue.indexOf("-") + 1, curentValue.lastIndexOf("-")));
            day = Integer.parseInt(curentValue.substring(0, curentValue.indexOf("-")));
        }

        birthDayPicker.init(year, (month - 1), day,
                new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker view,
                                              int year, int monthOfYear, int dayOfMonth) {
                        inputEditText.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                });
        birthDayPicker.setMaxDate(new Date().getTime());
    }

    private void initDisplay() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getIntent().getStringExtra("Tile"));
        inputEditText.setText(curentValue);
        switch (getIntent().getIntExtra("Type", 0)) {
            case R.integer.code_change_name:
                inputLayout.setHint("Input your name");
                break;
            case R.integer.code_change_mobile:
                inputLayout.setHint("Input your mobile phone");
                inputEditText.setInputType(InputType.TYPE_CLASS_PHONE);
                break;
            case R.integer.code_change_age:
                displayBirthDay();
                break;
            case R.integer.code_change_bio:
                inputLayout.setHint("Input your Bio");
                break;
            case R.integer.code_change_email:
                inputLayout.setHint("Input your Email");
                inputEditText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case R.integer.code_change_gender:
                displayGender();
                break;
            case R.integer.code_change_location:
                inputLayout.setHint("Input your location");
                break;

            case R.integer.code_change_password:
                displayPass();
                break;
            default:
                finish();
        }
    }

    private void displayBirthDay() {
        inputLayout.setHint("Input your birth day");
        inputEditText.setFocusable(false);
        inputLayout.setFocusable(false);
        birthDayPicker.setVisibility(View.VISIBLE);
        initDaypicker();
    }

    private void displayGender() {
        inputLayout.setVisibility(View.GONE);
        radioGroup.setVisibility(View.VISIBLE);
        if (curentValue.length() > 0) {
            switch (curentValue.toLowerCase()) {
                case "male":
                    male.setChecked(true);
                    break;
                case "female":
                    female.setChecked(true);
                    break;
                case "other":
                    other.setChecked(true);
                    break;
            }
        }
    }

    private void displayPass() {
        inputLayout.setHint("Current Password");
        mAuth = FirebaseAuth.getInstance();
        passLayout.setVisibility(View.VISIBLE);
        inputEditText.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD);
        inputEditText.setSelection(inputEditText.getText().length());

        pass.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD);
        pass.setSelection(inputEditText.getText().length());
    }

    private boolean variableData() {
        if (TypeChange == R.integer.code_change_password) {
            if (isValidField(inputEditText, 6)) {
                if (isValidField(pass, 6)) {
                    showProgess("Update password", "Please wait...");
                    CheckPassWord();
                } else {
                    Toast.makeText(this, "The new password is short", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "The current password is short", Toast.LENGTH_SHORT).show();
            }
        } else {
            switch (TypeChange) {
                case R.integer.code_change_gender:
                    changeGender();
                    break;
                case R.integer.code_change_location:
                    changeLocation();
                    break;
                case R.integer.code_change_name:
                    changeName();
                    break;
                case R.integer.code_change_age:
                    changeAge();
                    break;
                case R.integer.code_change_mobile:
                    changeMobile();
                    break;


            }
        }
        return true;
    }

    private void changeAge() {
        if (isValidField(inputEditText, 1)) {
            UpdateProfile("birhtDay", inputEditText.getText().toString());
            currentUser.setBirhtDay(inputEditText.getText().toString());
        } else
            Toast.makeText(this, "The birth day you input is invalid", Toast.LENGTH_SHORT).show();
    }

    private void changeMobile() {
        if (isValidField(inputEditText, 9)) {
            UpdateProfile("phoneNumber", inputEditText.getText().toString());
            currentUser.setPhoneNumber(inputEditText.getText().toString());
        } else
            Toast.makeText(this, "The phone number you input is invalid", Toast.LENGTH_SHORT).show();
    }


    private void changeName() {
        if (isValidField(inputEditText, 1)) {
            UpdateProfile("fullName", inputEditText.getText().toString());
            currentUser.setFullName(inputEditText.getText().toString());
        } else
            Toast.makeText(this, "The name you input is invalid", Toast.LENGTH_SHORT).show();
    }

    private void initControl() {
        inputEditText = (EditText) findViewById(R.id.editext);
        male = (RadioButton) findViewById(R.id.Male);
        female = (RadioButton) findViewById(R.id.Female);
        other = (RadioButton) findViewById(R.id.Other);
        radioGroup = (RadioGroup) findViewById(R.id.gender);
        inputLayout = (TextInputLayout) findViewById(R.id.inputLayout);
        pass = (EditText) findViewById(R.id.password);
        birthDayPicker = (DatePicker) findViewById(R.id.birthDayPicker);
        passLayout = (TextInputLayout) findViewById(R.id.passLayout);
    }

    private void initData() {
        curentValue = getIntent().getStringExtra("Value");
        currentUser = (User) getIntent().getSerializableExtra("User");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = FirebaseAuth.getInstance().getCurrentUser();
        TypeChange = getIntent().getIntExtra("Type", 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.change_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED, null);
                finish();
                return true;
            case R.id.done:
                variableData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void changeLocation() {
        if (isValidField(inputEditText, 1)) {
            currentUser.setLocation(inputEditText.getText().toString());
            UpdateProfile("location", inputEditText.getText().toString());
        } else
            Toast.makeText(this, "The location you input is invalid", Toast.LENGTH_SHORT).show();
    }

    private void changeGender() {
        boolean isMale = male.isChecked();
        boolean isFeMale = female.isChecked();
        boolean isOther = other.isChecked();
        if (isMale && isFeMale && isOther) {
            Toast.makeText(this, "Please choose your gender", Toast.LENGTH_SHORT).show();
        } else {
            if (isMale) {
                UpdateProfile("gender", "male");
                currentUser.setGender("male");
            } else if (isFeMale) {
                UpdateProfile("gender", "female");
                currentUser.setGender("female");
            } else {
                UpdateProfile("gender", "other");
                currentUser.setGender("other");
            }
        }
    }

    private void showProgess(String title, String message) {
        progDailog = ProgressDialog.show(this,
                title,
                message, true);
    }

    public boolean isValidField(EditText data, int length) {
        boolean check = true;
        if (((data.getText().toString()).trim()).length() < length) {
            check = false;
        }
        return check;
    }

    private void UpdateProfile(String type, String value) {
        showProgess("Updating profile", "Please wait");
        mDatabase.child("users").child(currentUser.getUid()).child(type).setValue(value)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progDailog.cancel();
                        Intent intent = new Intent();
                        intent.putExtra("User", currentUser);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ChangeProfileActivity.this, "Update failure", Toast.LENGTH_SHORT).show();
                        progDailog.cancel();
                    }
                });
    }

    private void CheckPassWord() {
        mAuth.signInWithEmailAndPassword(user.getEmail(), inputEditText.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            changePassWord();
                        } else {
                            Toast.makeText(ChangeProfileActivity.this, "The password you input is not correct", Toast.LENGTH_SHORT).show();
                            progDailog.cancel();
                        }
                    }
                });
    }

    private void changePassWord() {
        user.updatePassword(pass.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            progDailog.cancel();
                            setResult(RESULT_OK, null);
                            finish();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ChangeProfileActivity.this, "Error update password", Toast.LENGTH_SHORT).show();
                progDailog.cancel();
            }
        });
    }

}
