package tlive.techatrium.com.lsa.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import tlive.techatrium.com.lsa.CustomView.CustomViewPager;
import tlive.techatrium.com.lsa.Fragment.FavoriteFragment;
import tlive.techatrium.com.lsa.Fragment.HomeFragment;
import tlive.techatrium.com.lsa.Fragment.MessageFragment;
import tlive.techatrium.com.lsa.Fragment.ProfileFragment;
import tlive.techatrium.com.lsa.R;


public class MainActivity extends AppCompatActivity {

    private BottomNavigationViewEx bnv;
    private CustomViewPager pager;
    private ViewPagerAdapter adapter;

    private FirebaseStorage Storage;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private CallbackActivity callback;
    private ImageView floatingActionButton;
    private HomeFragment homeFragment;
    private FavoriteFragment favoriteFragment;
    private MessageFragment messageFragment;
    private ProfileFragment profileFragment;

    public static final String RTMP_BASE_URL = "rtmp://139.59.250.3/live/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initStyle();
        setContentView(R.layout.activity_main);
        initData();
        initControl();
        initEvent();
    }
    public void setDataReceivedListener(CallbackActivity listener){
        this.callback=listener;
    }
    private void initEvent() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.disableScroll(true);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grandPermission();
            }
        });
        bnv.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        pager.setCurrentItem(0);
                        return true;
                    case R.id.navigation_favorite:
                        pager.setCurrentItem(1);
                        return true;
                    case R.id.navigation_message:
                        pager.setCurrentItem(2);
                        return true;
                    case R.id.navigation_profile:
                        pager.setCurrentItem(3);
                        return true;
                }
                return true;
            }
        });
    }

    private void openStreamActivity() {
        Intent i = new Intent(MainActivity.this, SoftStreamingActivity.class);
        i.putExtra(BaseStreamingActivity.DIRECTION, true);
        i.putExtra("uid",mAuth.getUid());
        startActivity(i);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 9992){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                grandPermission();
            } else {
                Toast.makeText(this, "Grand permission to start", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private void grandPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    9992);
            return;
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    9992);
            return;
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.MODIFY_AUDIO_SETTINGS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.MODIFY_AUDIO_SETTINGS},
                    9992);
            return;
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.MODIFY_AUDIO_SETTINGS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.MODIFY_AUDIO_SETTINGS},
                    9992);
            return;
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    9992);
            return;
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    9992);
            return;
        }
        openStreamActivity();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 999){
            pager.setCurrentItem(3);
        }
        if(callback!=null){
            callback.onResult(data,requestCode,resultCode);
        }
    }

    private void initControl() {
        bnv = (BottomNavigationViewEx) findViewById(R.id.bnve);
        bnv.setTextVisibility(false);
        bnv.enableAnimation(false);
        bnv.enableShiftingMode(false);
        pager = (CustomViewPager) findViewById(R.id.content);
        floatingActionButton = (ImageView)findViewById(R.id.floatingActionButton);
    }

    private void initData() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        Storage = FirebaseStorage.getInstance("gs://tliveapp.appspot.com");
    }

    private void initStyle() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = getResources().getDrawable(R.drawable.bgrbtn);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.transparent2));
            window.setNavigationBarColor(getResources().getColor(R.color.transparent2));
            window.setBackgroundDrawable(background);
        }
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        public Fragment getItem(int num) {
            switch (num) {
                case 0:
                    if(homeFragment == null){
                       homeFragment = HomeFragment.getInstance (mDatabase,mAuth);
                    }
                    return homeFragment;
                case 1:
                    return new FavoriteFragment();
                case 2:
                    return new MessageFragment();
                case 3:
                    if(profileFragment == null) {
                        profileFragment = ProfileFragment.getInstance(mDatabase, Storage, mAuth);
                    }
                        return profileFragment;

            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Sezione " + position;
        }

    }
    public interface CallbackActivity {
       void onResult(Intent data, int RequestCode, int ResultCode);
    }
}
