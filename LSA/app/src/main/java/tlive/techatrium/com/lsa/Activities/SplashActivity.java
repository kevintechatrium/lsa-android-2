package tlive.techatrium.com.lsa.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import tlive.techatrium.com.lsa.R;

public class SplashActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private boolean WaitSplash, LoadAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initStyle();
        setContentView(R.layout.activity_splash);
//        initAuth
        mAuth = FirebaseAuth.getInstance();
        initUi();
    }

    private void initStyle() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = getResources().getDrawable(R.drawable.bgrbtn);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(R.color.transparent));
            window.setBackgroundDrawable(background);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void initUi() {
        new Thread(new Runnable() {
            public void run() {
                FirebaseUser currentUser = mAuth.getCurrentUser();
                updateUI(currentUser);
                LoadAuth = true;
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(1000);
                    WaitSplash = true;
                    if (LoadAuth) {
                        updateUI(mAuth.getCurrentUser());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private void updateUI(FirebaseUser currentUser) {
        if (WaitSplash) {
            if (currentUser != null) {
                startActivity(new Intent(this, MainActivity.class));
                finish();
//            Toast.makeText(this, "Da dang nhap", Toast.LENGTH_SHORT).show();
            } else {
                startActivity(new Intent(this, Login.class));
                finish();
            }
        }

    }

}
