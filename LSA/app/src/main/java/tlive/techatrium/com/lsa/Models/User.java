package tlive.techatrium.com.lsa.Models;

import java.io.Serializable;

/**
 * Created by hongt on 31/10/2017.
 */

public class User implements Serializable {
    private String avatar ="",
            birhtDay="",
            coverImage="",
            deviceToken="",
            email="",
            fullName="",
            gender="",
            location="",
            phoneNumber="",
            provider="",
            uid="",
            t_LiveId = "";
    private int fans = 0,
            following = 0,
            level = 0;
    private Wallet wallet;
    private long createdAt;

    public String getT_LiveId() {
        return t_LiveId;
    }

    public void setT_LiveId(String t_LiveId) {
        this.t_LiveId = t_LiveId;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public String getAvatar() {
        return avatar;
    }

    public User() {
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBirhtDay() {
        return birhtDay;
    }

    public void setBirhtDay(String birhtDay) {
        this.birhtDay = birhtDay;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getFans() {
        return fans;
    }

    public void setFans(int fans) {
        this.fans = fans;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}
