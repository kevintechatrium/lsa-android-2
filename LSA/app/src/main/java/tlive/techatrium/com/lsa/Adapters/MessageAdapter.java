package tlive.techatrium.com.lsa.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tlive.techatrium.com.lsa.Models.MessageStream;
import tlive.techatrium.com.lsa.R;

/**
 * Created by hongt on 09/11/2017.
 */

public class MessageAdapter extends ArrayAdapter<MessageStream> {
    Context mC;
    ArrayList<MessageStream> mL = new ArrayList<MessageStream>();

    public MessageAdapter(Context context, int resource, List<MessageStream> objects) {
        super(context, resource, objects);
        this.mC = context;
        this.mL = new ArrayList<MessageStream>(objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowview = convertView;
        viewItem hv = null;
        if (rowview == null) {
            LayoutInflater ift = (LayoutInflater) mC.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = ift.inflate(R.layout.msg_item, null);
            hv = new viewItem();
            hv.name = (TextView) rowview.findViewById(R.id.name);
            hv.msg = (TextView) rowview.findViewById(R.id.msg);
            hv.layoutMesg = (LinearLayout)rowview.findViewById(R.id.messageLayout);
            hv.news = (TextView)rowview.findViewById(R.id.news);
            rowview.setTag(hv);
        } else {
            hv = (viewItem) convertView.getTag();
        }
        MessageStream streamModel = mL.get(position);
        if(streamModel.getType().equals("news")){
            hv.news.setText(streamModel.getText());
            hv.news.setVisibility(View.VISIBLE);
            hv.layoutMesg.setVisibility(View.GONE);
        }
        else {
            hv.msg.setText(streamModel.getText());
            hv.name.setText(streamModel.getUser().getFullName());
            hv.layoutMesg.setVisibility(View.VISIBLE);
            hv.news.setVisibility(View.GONE);
        }

        return rowview;

    }

    static class viewItem {
        TextView msg, name,news;
        LinearLayout layoutMesg;
    }

    public void Wrap_Items(ArrayList<MessageStream> items) {
        this.mL = items;
        notifyDataSetChanged();
    }
}