package tlive.techatrium.com.lsa.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import tlive.techatrium.com.lsa.Models.User;
import tlive.techatrium.com.lsa.Models.Wallet;
import tlive.techatrium.com.lsa.R;

public class Register extends AppCompatActivity implements View.OnClickListener {
    private EditText email,password,phone,name;
    private TextView agreement,login;
    private Button SignUp;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private static String TAG = "LSADebug";
    private ProgressDialog progDailog;
    private ImageView login_background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initStyle();
        setContentView(R.layout.activity_register);
        initControl();
        initEvent();
        initData();
    }

    private void initData() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    private void initControl() {
        name = (EditText)findViewById(R.id.name);
        phone = (EditText)findViewById(R.id.phone);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.pass);
        agreement = (TextView)findViewById(R.id.agreement);
        SignUp = (Button)findViewById(R.id.sign_up);
        login = (TextView) findViewById(R.id.login);
        login_background =(ImageView)findViewById(R.id.login_background);
    }

    private void initEvent() {
        agreement.setOnClickListener(this);
        SignUp.setOnClickListener(this);
        login.setOnClickListener(this);
        login_background.setOnClickListener(this);

    }

    private void initStyle() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = getResources().getDrawable(R.drawable.bgrbtn);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.agreement:
                bntagreement();
                break;
            case R.id.sign_up:
                bntsign_up();
                break;
            case R.id.login:
                bntlogin();
                break;
            case R.id.login_background:
                hideSoftKeyboard(this);
                break;

        }
    }

    private void bntsign_up() {
        if(variableData()){
            ProgressDialog();
            mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("SignUp", "createUserWithEmail:success");
                                createUser(true);

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("SignUp", "createUserWithEmail:failure", task.getException());
                                Toast.makeText(Register.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                progDailog.cancel();
                            }
                        }
                    });
        }

    }

    private boolean variableData() {
        if(!isValidField(name.getText().toString(),1)){
            showToast("Name is invalid");
            return false;
        }
        else if(!isValidEmailAddress(email.getText().toString())) {
            showToast("Name Email invalid");
            return false;
        }
        else if(!isValidField(password.getText().toString(),6)){
            showToast("The Password must be at least 6 characters");
            return false;
        }
        return true;
    }

    private void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }


    private void ProgressDialog() {
        progDailog = ProgressDialog.show(this,
                "Please, Wait",
                "Authenticating....", true);
    }

    private void createUser(boolean Provider) {
        FirebaseUser user = mAuth.getCurrentUser();
        User info = new User();
        info.setEmail(user.getEmail());
        info.setUid(user.getUid());
        info.setFans(0);
        info.setFollowing(0);
        info.setLevel(0);
        info.setFullName(name.getText().toString());
        info.setPhoneNumber(phone.getText().toString());
        info.setWallet(new Wallet(1000000000,0));
        if(Provider){
            info.setProvider("email");
        }
        else {
            info.setProvider("facebook");
        }
        updateInfo(user,info);
    }

    private void updateInfo(final FirebaseUser user,User info ) {
        mDatabase.child("users").child(user.getUid()).setValue(info).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mDatabase.child("users").child(user.getUid()).child("createdAt").setValue(ServerValue.TIMESTAMP).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                progDailog.cancel();
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("result",true);
                                setResult(Activity.RESULT_OK,returnIntent);
                                startActivity(new Intent(Register.this, MainActivity.class));
                                finish();
                            }
                            else {
                                Log.d(TAG, "onComplete: ", task.getException());
                            }
                        }
                    });
                }
                else {
                    Log.d(TAG, "onComplete: ", task.getException());
                }
            }
        });
    }

    private void bntagreement() {

    }

    private void bntlogin() {
        finish();
    }
    public boolean isValidField(String data,int length){
        boolean check = true;
            if((data.trim()).length() < length){
                check = false;
            }
        return check;
    }

    public static boolean isValidEmailAddress(String email) {
        boolean stricterFilter = true;
        String stricterFilterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        String laxString = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
        String emailRegex = stricterFilter ? stricterFilterString : laxString;
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(emailRegex);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}
