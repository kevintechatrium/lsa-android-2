package tlive.techatrium.com.lsa.Models;

import java.io.Serializable;

/**
 * Created by hongt on 01/11/2017.
 */

public class Wallet implements Serializable {
    private int diamon_buy, diamon_recieve;

    public int getDiamon_buy() {
        return diamon_buy;
    }

    public void setDiamon_buy(int diamon_buy) {
        this.diamon_buy = diamon_buy;
    }

    public int getDiamon_recieve() {
        return diamon_recieve;
    }

    public void setDiamon_recieve(int diamon_recieve) {
        this.diamon_recieve = diamon_recieve;
    }

    public Wallet(int diamon_buy, int diamon_recieve) {
        this.diamon_buy = diamon_buy;
        this.diamon_recieve = diamon_recieve;
    }

    public Wallet() {
    }
}
