package tlive.techatrium.com.lsa.Adapters

import android.app.Dialog
import android.app.PendingIntent.getActivity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

import com.squareup.picasso.Picasso

import java.util.ArrayList
import java.util.Collections

import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.view_user.*
import tlive.techatrium.com.lsa.Models.User
import tlive.techatrium.com.lsa.R

/**
 * Created by hongt on 13/11/2017.
 */

class ViewUserStreamAdapter// data is passed into the constructor
(context: Context, private var mUsers: List<User>?) : RecyclerView.Adapter<ViewUserStreamAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater
    private var mClickListener: ItemClickListener? = null
    private var mBottomSheetDialog : Dialog? = null
    private var context : Context? = null
    var position = 0

    init {
        this.mInflater = LayoutInflater.from(context)
        this.context = context
    }

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the view and textview in each row
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = mUsers!![position]
        if (user.avatar.length != 0) {
            Picasso.with(mInflater.context).load(user.avatar).into(holder.avatar)
        }
        this.position = position
    }

    // total number of rows
    override fun getItemCount(): Int {
        return mUsers!!.size
    }

    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var avatar: CircleImageView

        init {
            avatar = itemView.findViewById(R.id.avatar)
            avatar.setOnClickListener {
                creatdialog() }
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            if (mClickListener != null) mClickListener!!.onItemClick(view, adapterPosition)
        }
    }

    // convenience method for getting data at click position
    fun getItem(id: Int): User {
        return mUsers!![id]
    }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    fun wrapItems(users: ArrayList<User>) {
        this.mUsers = users
        notifyDataSetChanged()
    }

    private fun creatdialog() {
        val view = mInflater.inflate(R.layout.view_user, null)
        mBottomSheetDialog = Dialog(context,
                R.style.MaterialDialogSheet)
        mBottomSheetDialog?.setContentView(view)
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.getWindow()!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
        mBottomSheetDialog?.getWindow()!!.setGravity(Gravity.BOTTOM)
        mBottomSheetDialog?.show()
        if(mUsers?.get(position)!!.avatar != null)
        Picasso.with(context).load(mUsers?.get(position)!!.avatar).into(mBottomSheetDialog?.findViewById<CircleImageView>(R.id.avatar))
        mBottomSheetDialog?.findViewById<TextView>(R.id.name)!!.text = mUsers!!.get(position)!!.fullName
        mBottomSheetDialog?.findViewById<TextView>(R.id.location)!!.text = mUsers!!.get(position)!!.location
        mBottomSheetDialog?.findViewById<TextView>(R.id.t_id)!!.text = "ID: "+ mUsers!!.get(position)!!.t_LiveId
        mBottomSheetDialog?.findViewById<TextView>(R.id.fan)!!.text = "" + mUsers!!.get(position)!!.fans
        mBottomSheetDialog?.findViewById<TextView>(R.id.follow)!!.text = ""+ mUsers!!.get(position)!!.following
        mBottomSheetDialog?.close!!.setOnClickListener { mBottomSheetDialog!!.cancel() }

    }
}