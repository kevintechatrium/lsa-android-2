package tlive.techatrium.com.lsa.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import java.util.UUID;

import me.lake.librestreaming.client.RESClient;
import me.lake.librestreaming.core.listener.RESConnectionListener;
import me.lake.librestreaming.core.listener.RESVideoChangeListener;
import me.lake.librestreaming.filter.softaudiofilter.BaseSoftAudioFilter;
import me.lake.librestreaming.model.RESConfig;
import me.lake.librestreaming.model.Size;
import tlive.techatrium.com.lsa.Models.StreamModel;
import tlive.techatrium.com.lsa.Models.User;
import tlive.techatrium.com.lsa.R;
import tlive.techatrium.com.lsa.audiofilter.SetVolumeAudioFilter;
import tlive.techatrium.com.lsa.ui.AspectTextureView;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN;
import static android.view.WindowManager.LayoutParams.SOFT_INPUT_IS_FORWARD_NAVIGATION;

/**
 * Created by lake on 16-5-31.
 */
public abstract class BaseStreamingActivity extends AppCompatActivity implements RESConnectionListener,
        TextureView.SurfaceTextureListener,
        View.OnClickListener,
        RESVideoChangeListener {
    private static final String TAG = "RES";
    public static final String DIRECTION = "direction";
    protected RESClient resClient;
    protected AspectTextureView txv_preview;
    protected Handler mainHander;
    protected TextView btn_toggle, tag;
    private EditText title;
    protected boolean started = false;
    protected String rtmpaddr = "rtmp://139.59.250.3/live/";
    protected int filtermode = RESConfig.FilterMode.SOFT;
    protected DatabaseReference mDatabase, UserRef, StreamRef;
    protected String currentStreamID;
    private Dialog dialog;
    private LinearLayout contentStart;
    private ListView listTag;
    private String[] arrayList;
    private ArrayAdapter adapter;
    private FirebaseStorage storage;
    private ImageView userCover;
    private StreamModel streamModel;
    protected User currentUser;
    RESConfig resConfig;

    private ProgressDialog progDailog;
    private StorageReference storageRef;
    private StorageMetadata metadata;
    protected ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initStyle();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streaming);
        initControl();
        initData();
        configCamera();
        initEvent();
        getWindow().setSoftInputMode(SOFT_INPUT_IS_FORWARD_NAVIGATION);
    }


    private void initStyle() {
        Intent i = getIntent();
        if (i.getBooleanExtra(DIRECTION, false)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        started = false;
    }

    private void configCamera() {
        resConfig = RESConfig.obtain();
        resConfig.setFilterMode(filtermode);
        resConfig.setTargetVideoSize(new Size(835, 480));
        resConfig.setBitRate(750 * 1024);
        resConfig.setVideoFPS(20);
        resConfig.setVideoGOP(1);
        resConfig.setVideoBufferQueueNum(1);
        resConfig.setRenderingMode(RESConfig.RenderingMode.OpenGLES);
        resConfig.setDefaultCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
        int frontDirection, backDirection;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, cameraInfo);
        frontDirection = cameraInfo.orientation;
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, cameraInfo);
        backDirection = cameraInfo.orientation;
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            resConfig.setFrontCameraDirectionMode((frontDirection == 90 ? RESConfig.DirectionMode.FLAG_DIRECTION_ROATATION_270 : RESConfig.DirectionMode.FLAG_DIRECTION_ROATATION_90) | RESConfig.DirectionMode.FLAG_DIRECTION_FLIP_HORIZONTAL);
            resConfig.setBackCameraDirectionMode((backDirection == 90 ? RESConfig.DirectionMode.FLAG_DIRECTION_ROATATION_90 : RESConfig.DirectionMode.FLAG_DIRECTION_ROATATION_270));
        } else {
            resConfig.setBackCameraDirectionMode((backDirection == 90 ? RESConfig.DirectionMode.FLAG_DIRECTION_ROATATION_0 : RESConfig.DirectionMode.FLAG_DIRECTION_ROATATION_180));
            resConfig.setFrontCameraDirectionMode((frontDirection == 90 ? RESConfig.DirectionMode.FLAG_DIRECTION_ROATATION_180 : RESConfig.DirectionMode.FLAG_DIRECTION_ROATATION_0) | RESConfig.DirectionMode.FLAG_DIRECTION_FLIP_HORIZONTAL);
        }
        if (!resClient.prepare(resConfig)) {
            resClient = null;
            Toast.makeText(this, "Prepare failed", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        Size s = resClient.getVideoSize();
        txv_preview.setAspectRatio(AspectTextureView.MODE_OUTSIDE, ((double) s.getWidth()) / s.getHeight());
        Log.d(TAG, "version=" + resClient.getVertion());
        resClient.setConnectionListener(this);
        resClient.setVideoChangeListener(this);

        mainHander = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//                tv_speed.setText("byteSpeed=" + (resClient.getAVSpeed() / 1024) + ";drawFPS=" + resClient.getDrawFrameRate() + ";sendFPS=" + resClient.getSendFrameRate() + ";sendbufferfreepercent=" + resClient.getSendBufferFreePercent());
                sendEmptyMessageDelayed(0, 3000);
                if (resClient.getSendBufferFreePercent() <= 0.05) {
                    Toast.makeText(BaseStreamingActivity.this, "Netspeed is low!", Toast.LENGTH_SHORT).show();
                }
            }
        };
        mainHander.sendEmptyMessage(0);

        resClient.setSoftAudioFilter(new SetVolumeAudioFilter());

    }


    private void initEvent() {
        tag.setOnClickListener(this);
        btn_toggle.setOnClickListener(this);
        userCover.setOnClickListener(this);
        txv_preview.setKeepScreenOn(true);
        txv_preview.setSurfaceTextureListener(this);
        findViewById(R.id.cancel).setOnClickListener(this);
        contentStart.setOnClickListener(this);
    }

    protected void initControl() {
        txv_preview = (AspectTextureView) findViewById(R.id.txv_preview);
        title = (EditText) findViewById(R.id.title);
        btn_toggle = (TextView) findViewById(R.id.btn_toggle);
        tag = (TextView) findViewById(R.id.tag);
        contentStart = (LinearLayout) findViewById(R.id.contentStart);
        userCover = (ImageView) findViewById(R.id.userCover);
        pager = (ViewPager) findViewById(R.id.viewPager);
        txv_preview.setKeepScreenOn(true);
        txv_preview.setSurfaceTextureListener(this);

    }

    private void initData() {
        resClient = new RESClient();
        storage = FirebaseStorage.getInstance("gs://tliveapp.appspot.com");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        UserRef = mDatabase.child("users").child(getIntent().getStringExtra("uid"));
        StreamRef = mDatabase.child("stream_live_room");
        arrayList = getResources().getStringArray(R.array.tag);
        storageRef = storage.getReference();
        metadata = new StorageMetadata.Builder()
                .setContentType("image/jpg")
                .build();
        showProgess("Loading", "Please wait...");
        UserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentUser = dataSnapshot.getValue(User.class);
                loadCoverPickture();
                progDailog.cancel();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(BaseStreamingActivity.this, "Error", Toast.LENGTH_SHORT).show();
                progDailog.cancel();
                finish();
            }
        });
    }

    private void loadCoverPickture() {
        if (currentUser.getCoverImage().length() > 1) {
            Picasso.with(this).load(currentUser.getCoverImage()).into(userCover);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if(currentStreamID!=null){
            StreamRef.child(currentStreamID).child("status").setValue("ended").addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                }
            });
        }
        if (mainHander != null) {
            mainHander.removeCallbacksAndMessages(null);
        }
        if (started) {
            resClient.stopStreaming();
        }
        if (resClient != null) {
            resClient.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onOpenConnectionResult(int result) {
        if (result == 0) {
            Log.e(TAG, "server IP = " + resClient.getServerIpAddr());
        } else {
            Toast.makeText(this, "Start failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onWriteError(int errno) {
        if (errno == 9) {
            resClient.stopStreaming();
            resClient.startStreaming(rtmpaddr + currentStreamID);
            Toast.makeText(this, "errno==9, Restarting", Toast.LENGTH_SHORT).show();
        }
        /**
         * failed to write data,maybe restart.
         */
    }

    @Override
    public void onCloseConnectionResult(int result) {
        /**
         * result==0 success
         * result!=0 failed
         */
    }

    protected SurfaceTexture texture;
    protected int sw, sh;

    @Override
    public void onVideoSizeChanged(int width, int height) {
        txv_preview.setAspectRatio(AspectTextureView.MODE_OUTSIDE, ((double) width) / height);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        if (resClient != null) {
            resClient.startPreview(surface, width, height);
        }
        texture = surface;
        sw = width;
        sh = height;
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        if (resClient != null) {
            resClient.updatePreview(width, height);
        }
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (resClient != null) {
            resClient.stopPreview(true);
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_toggle:
                startStream();
                break;
            case R.id.tag:
                openDialogTag();
                break;
            case R.id.userCover:
                pickImage();
                break;
            case R.id.cancel:
                stopStream();
                finish();
                break;
        }
    }

    private void startStream() {
        showProgess("Starting", "Please wait...");

        currentStreamID = StreamRef.push().getKey();
        UserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentUser = dataSnapshot.getValue(User.class);
                createStream();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progDailog.cancel();
            }
        });
    }

    private void createStream() {
        streamModel = new StreamModel();
        streamModel.setKey(currentStreamID);
        streamModel.setOwner(currentUser);
        streamModel.setTag(tag.getText().toString());
        streamModel.setTitle(title.getText().toString());
        streamModel.setStatus("live");
        StreamRef.child(currentStreamID).setValue(streamModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                StreamRef.child(currentStreamID).child("createdAt").setValue(ServerValue.TIMESTAMP).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!started) {
                            resClient.startStreaming(rtmpaddr + currentStreamID);
                            onStartStream();
                            started = true;
                        }
                        progDailog.cancel();
                        contentStart.setVisibility(View.GONE);
                    }
                });
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progDailog.cancel();
                    }
                });
    }

    public abstract void  onStartStream();

    private void stopStream() {
        if (started) {
            resClient.stopStreaming();
            started = false;
        }
    }

    private void pickImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setAutoZoomEnabled(true)
                .setOutputCompressQuality(50)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                uploadPhoto(result);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        if(requestCode == 9992){

        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    private void uploadPhoto(final CropImage.ActivityResult result) {

        UploadTask uploadTask = storageRef.child("cover_photos/" + createTransactionID()).putFile(result.getUri(), metadata);
        showProgess("Uploading", "Please wait...");


        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progDailog.cancel();
            }
        })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        updateProfilePhoto(taskSnapshot.getDownloadUrl() + "", result.getUri());

                    }
                });
    }

    private String createTransactionID() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }

    private void showProgess(String title, String message) {
        progDailog = ProgressDialog.show(this,
                title,
                message, true);
    }

    private void updateProfilePhoto(String url, final Uri image) {

        UserRef.child("coverImage").setValue(url)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progDailog.cancel();
                        userCover.setImageURI(image);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(BaseStreamingActivity.this, "Update failure", Toast.LENGTH_SHORT).show();
                        progDailog.cancel();
                    }
                });
    }

    private void openDialogTag() {
        if (dialog == null) {
            dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialog.setContentView(R.layout.seclect_tag_layout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            listTag = (ListView) dialog.findViewById(R.id.list_tag);
            adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_list_item_1, arrayList) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    // Get the Item from ListView
                    View view = super.getView(position, convertView, parent);
                    // Initialize a TextView for ListView each Item
                    TextView tv = (TextView) view.findViewById(android.R.id.text1);

                    // Set the text color of TextView (ListView Item)
                    tv.setTextColor(Color.WHITE);
                    // Generate ListView Item using TextView
                    return view;
                }
            };
            listTag.setAdapter(adapter);
            listTag.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    dialog.cancel();
                    tag.setText(arrayList[position]);
                }
            });
        }
        dialog.show();
    }

}
