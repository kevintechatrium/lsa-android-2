package tlive.techatrium.com.lsa.Fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import me.lake.librestreaming.client.RESClient;
import tlive.techatrium.com.lsa.Activities.SplashActivity;
import tlive.techatrium.com.lsa.Adapters.MessageAdapter;
import tlive.techatrium.com.lsa.Adapters.ViewUserStreamAdapter;
import tlive.techatrium.com.lsa.Models.MessageStream;
import tlive.techatrium.com.lsa.Models.StreamModel;
import tlive.techatrium.com.lsa.Models.User;
import tlive.techatrium.com.lsa.R;
import tlive.techatrium.com.lsa.softfilter.SkinBlurFilterSoft;

/**
 * Created by hongt on 01/11/2017.
 */

@SuppressLint("ValidFragment")
public class StreamFragment extends Fragment implements View.OnClickListener {
    private View view;
    private static StreamFragment streamFragment;
    private DatabaseReference mDatabase;
    private ListView listMsg;
    private RecyclerView listAvatar;
    private TextView name, currentViewers, currentLikes;
    private CircleImageView userAvatar;
    private ImageView follow_detail, send, btn_setting, btn_beauty, btn_swap_camera, btn_like;
    private EditText edt_comment;
    private RESClient resClient;
    private SkinBlurFilterSoft soft;
    private boolean filter = true;
    private boolean isFront = true, isHide = true;
    private LinearLayout layoutSetting;
    private String currentStreamID;
    private ChildEventListener listener, room;
    private ArrayList<MessageStream> arrMsg = new ArrayList<>();
    private MessageAdapter adapterMsg;
    private MessageStream msg;
    private User currentUser;
    private Animation fly[] = new Animation[10];
    private FrameLayout floatLike;
    private StreamModel streamModel;
    private ImageView imageView[] = new ImageView[10];
    private boolean booleans[] = new boolean[10];
    private ArrayList<User> users = new ArrayList<>();
    private ViewUserStreamAdapter adapter;
    private Dialog mBottomSheetDialog;

    private StreamFragment(DatabaseReference mDatabase, RESClient resClient, String currentStreamID, User currentUser) {
        this.mDatabase = mDatabase;
        this.resClient = resClient;
        this.currentStreamID = currentStreamID;
        this.currentUser = currentUser;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.content_control, container, false);
            initControl();
            initEvent();
            initDisplay();
            initData();
        }
        return view;
    }

    private void like() {
        if (streamModel != null) {
            mDatabase.child("stream_live_room").child(currentStreamID).child("totalLikes").setValue((streamModel.getTotalLikes() + 1));
        }
    }

    private void creatdialog(){
        View view = getLayoutInflater().inflate(R.layout.view_user, null);
        mBottomSheetDialog = new Dialog(getActivity(),
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }
    private void initDisplay() {
        adapterMsg = new MessageAdapter(getActivity(), R.layout.msg_item, arrMsg);
        listMsg.setAdapter(adapterMsg);
    }

    private void initData() {
        soft = new SkinBlurFilterSoft(getActivity());
        mDatabase.child("chat_live_room").child(currentStreamID).addChildEventListener(listener);
        mDatabase.child("stream_live_room").orderByChild("key").equalTo(currentStreamID).addChildEventListener(room);
        getUserInRoom();
        Log.d("DebugLSA", "initData: " + currentStreamID);
        for (int i = 0; i < 10; i++) {
            imageView[i] = (ImageView) floatLike.getChildAt(i);
        }
        fly[0] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim);
        fly[1] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim1);
        fly[2] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim2);
        fly[3] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim3);
        fly[4] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim4);
        fly[5] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim5);
        fly[6] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim6);
        fly[7] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim7);
        fly[8] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim8);
        fly[9] = AnimationUtils.loadAnimation(getActivity(), R.anim.fly_anim9);
    }

    private void initEvent() {
        follow_detail.setOnClickListener(this);
        send.setOnClickListener(this);
        btn_setting.setOnClickListener(this);
        btn_beauty.setOnClickListener(this);
        btn_swap_camera.setOnClickListener(this);
        btn_like.setOnClickListener(this);
        view.findViewById(R.id.like).setOnClickListener(this);
        edt_comment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    send.setVisibility(View.VISIBLE);
                else
                    send.setVisibility(View.GONE);
            }
        });
        room = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                streamModel = dataSnapshot.getValue(StreamModel.class);
                showInfo();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                streamModel = dataSnapshot.getValue(StreamModel.class);
                showInfo();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    msg = dataSnapshot.getValue(MessageStream.class);
                    if(msg.getType().equals("hi")){
                        getUserInRoom();
                    }
                    else {
                        arrMsg.add(msg);
                        UpdateMessages();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void showInfo() {
        if (!(currentLikes.getText().toString().equals(streamModel.getTotalLikes() + "")))
            for (int i = 0; i < 10; i++) {
                final int x = i;
                if (!booleans[i]) {
                    booleans[i] = true;
                    imageView[i].setVisibility(View.VISIBLE);
                    imageView[i].startAnimation(fly[i]);
                    imageView[i].postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            imageView[x].setVisibility(View.GONE);
                            booleans[x] = false;
                        }
                    }, 2000);
                    break;
                }
            }
        currentLikes.setText(streamModel.getTotalLikes() + "");

    }

    private void UpdateMessages() {
        adapterMsg.Wrap_Items(this.arrMsg);
    }

    private void initControl() {
        listMsg = (ListView) view.findViewById(R.id.listMsg);
        listAvatar = (RecyclerView) view.findViewById(R.id.usersAvatar);
        name = (TextView) view.findViewById(R.id.name);
        currentViewers = (TextView) view.findViewById(R.id.currentViewers);
        currentLikes = (TextView) view.findViewById(R.id.currentLikes);
        userAvatar = (CircleImageView) view.findViewById(R.id.userAvatar);

        follow_detail = (ImageView) view.findViewById(R.id.follow_detail);
        send = (ImageView) view.findViewById(R.id.send);
        btn_setting = (ImageView) view.findViewById(R.id.btn_setting);
        btn_beauty = (ImageView) view.findViewById(R.id.btn_beauty);
        btn_swap_camera = (ImageView) view.findViewById(R.id.btn_swap_camera);
        btn_like = (ImageView) view.findViewById(R.id.btn_like);
        edt_comment = (EditText) view.findViewById(R.id.edt_comment);
        layoutSetting = (LinearLayout) view.findViewById(R.id.layoutSetting);
        floatLike = (FrameLayout) view.findViewById(R.id.animation);
        adapter = new ViewUserStreamAdapter(getActivity(),users);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        listAvatar.setLayoutManager(layoutManager);
        listAvatar.setAdapter(adapter);
    }

    public static StreamFragment getInstance(DatabaseReference mDatabase, RESClient resClient, String currentStreamID, User currentUser) {
        streamFragment = streamFragment == null ? new StreamFragment(mDatabase, resClient, currentStreamID, currentUser) : streamFragment;
        return streamFragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_swap_camera:
                resClient.swapCamera();
                if (isFront) {
                    btn_swap_camera.setImageResource(R.drawable.ic_camerabackx);
                } else {
                    btn_swap_camera.setImageResource(R.drawable.ic_camerafrontx);
                }
                isFront = !isFront;
                break;
            case R.id.btn_beauty:
                if (filter) {
                    resClient.setSoftVideoFilter(null);
                    btn_beauty.setImageResource(R.drawable.ic_beautyoffx);
                } else {
                    resClient.setSoftVideoFilter(soft);
                    btn_beauty.setImageResource(R.drawable.ic_beautyonx);
                }
                filter = !filter;
                break;
            case R.id.send:
                sendMessages();
                break;
            case R.id.btn_setting:
                switchSetting(false);
                break;
//            case R.id.btn_like:
//                like();
//                break;
//            case R.id.like:
//                like();
//                break;
        }
    }

    private void sendMessages() {
        if ((edt_comment.getText() + "").length() > 0) {
            MessageStream msg = new MessageStream();
            msg.setText(edt_comment.getText().toString());
            msg.setUser(currentUser);
            msg.setType("comment");
            String key = mDatabase.child("chat_live_room").child(currentStreamID).push().getKey();
            mDatabase.child("chat_live_room").child(currentStreamID).child(key).setValue(msg);
            mDatabase.child("chat_live_room").child(currentStreamID).child(key).child("createdAt").setValue(ServerValue.TIMESTAMP);
            edt_comment.setText("");
        } else {
            Toast.makeText(getActivity(), "Input your comment", Toast.LENGTH_SHORT).show();
        }
    }

    private void switchSetting(boolean hide) {
        if (hide) {
            layoutSetting.setVisibility(View.GONE);
            isHide = true;
        } else {
            if (isHide)
                layoutSetting.setVisibility(View.VISIBLE);
            else
                layoutSetting.setVisibility(View.GONE);
            isHide = !isHide;
        }
    }

    public void getUserInRoom() {
        mDatabase.child("viewers_on_room").child(currentStreamID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                users.clear();
                for (DataSnapshot data:dataSnapshot.getChildren()) {
                    users.add(data.getValue(User.class));
                }
                adapter.wrapItems(users);
                Log.d("ok", "onDataChange: "+users.size());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}