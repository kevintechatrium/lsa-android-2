package tlive.techatrium.com.lsa.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import tlive.techatrium.com.lsa.Models.User;
import tlive.techatrium.com.lsa.Models.Wallet;
import tlive.techatrium.com.lsa.R;

public class Login extends AppCompatActivity implements View.OnClickListener  {

    private EditText email,password;
    private TextView agreement,signUp;
    private Button Login, forgot;
    private FirebaseAuth mAuth;
    private CallbackManager mCallbackManager;
    private LoginButton loginButton;
    private AlertDialog forgotDialog;
    private DatabaseReference mDatabase;
    private static String TAG = "LSADebug";
    private ProgressDialog progDailog;
    private ImageView login_background;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initStyle();
        setContentView(R.layout.activity_login);
        //     initAuth
        mAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        initControl();
        initEvent();
    }



    private void initControl() {
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.pass);
        agreement = (TextView)findViewById(R.id.agreement);
        signUp = (TextView)findViewById(R.id.sign_up);
        Login = (Button) findViewById(R.id.login);
        forgot = (Button)findViewById(R.id.forgot);
        loginButton = findViewById(R.id.login_button);
        login_background =(ImageView)findViewById(R.id.login_background);
    }

    private void initEvent() {
        agreement.setOnClickListener(this);
        signUp.setOnClickListener(this);
        Login.setOnClickListener(this);
        forgot.setOnClickListener(this);
        login_background.setOnClickListener(this);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("Main", response.toString());
                                handleFacebookAccessToken(loginResult.getAccessToken(),object);
                                LoginManager.getInstance().logOut();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==999 && resultCode == Activity.RESULT_OK){
            finish();
        }

    }
    private void initStyle() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = getResources().getDrawable(R.drawable.bgrbtn);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        }
    }
    private void handleFacebookAccessToken(AccessToken token, final JSONObject object) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            updateInfo(object);
                        } else {
                            Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                            Log.d("fbLogin", "onComplete: "+task.getException());
                        }
                    }
                });
    }

    private void updateInfo(final JSONObject object) {
        ProgressDialog();
        final FirebaseUser user = mAuth.getCurrentUser();
        final User info = new User();
        mDatabase.child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            User data;
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.d(TAG, "onDataChange: "+ snapshot);
                data = snapshot.getValue(User.class);
                if (data == null) {
                    try {
                        info.setEmail(user.getEmail());
                        info.setUid(user.getUid());
                        info.setFans(0);
                        info.setFollowing(0);
                        info.setLevel(0);
                        info.setFullName(object.getString("name"));
                        info.setWallet(new Wallet(1000000000,0));
                        info.setProvider("facebook");
                        PostInfo(user,info);
//        updateUI(user);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    updateUI(user);
                    progDailog.cancel();
                }
                mDatabase.child("users").child(user.getUid()).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progDailog.cancel();
            }

        });

    }

    private void PostInfo(final FirebaseUser user, User info) {
        mDatabase.child("users").child(user.getUid()).setValue(info).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mDatabase.child("users").child(user.getUid()).child("createdAt").setValue(ServerValue.TIMESTAMP).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                progDailog.cancel();
                                Toast.makeText(Login.this, "Success",
                                        Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Log.d(TAG, "onComplete: ", task.getException());
                                showToast("Authenticating Error ");
                                progDailog.cancel();
                            }
                        }
                    });
                }
                else {
                    Log.d(TAG, "onComplete: ", task.getException());
                    showToast("Authenticating Error ");
                }
                progDailog.cancel();
            }
        });
    }
    private void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }


    private void ProgressDialog() {
        progDailog = ProgressDialog.show(this,
                "Please, Wait",
                "Authenticating....", true);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.agreement:
                bntagreement();
                break;
            case R.id.sign_up:
                bntsign_up();
                break;
            case R.id.login:
                bntlogin();
                break;
            case R.id.forgot:
                bntforgot();
                break;
            case R.id.login_background:
                hideSoftKeyboard(this);
                break;
        }
    }

    private void bntforgot() {
        if(forgotDialog == null){
            BuildDialog();
        }
        else {
            forgotDialog.show();
        }
    }

    private void BuildDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        LayoutInflater inflater=getLayoutInflater();
        //this is what I did to added the layout to the alert dialog
        final View layout=inflater.inflate(R.layout.content_dialog_forgot,null);
        dialog.setView(layout);
        dialog.setTitle("Enter your Email");
        dialog.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                String email = ((EditText)layout.findViewById(R.id.email)).getText().toString();
                if(isValidEmailAddress(email)){
                    mAuth.sendPasswordResetEmail(email)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(Login.this, "Success", Toast.LENGTH_SHORT).show();
                                    }
                                    else{
                                        Toast.makeText(Login.this, "Fail", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
                else {
                    Toast.makeText(Login.this, "Email invalid", Toast.LENGTH_SHORT).show();
                }

            }
        })
                .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        forgotDialog = dialog.create();
        forgotDialog.show();
    }



    private void bntlogin() {
        if(variableData()){
            mAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("Auth", "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                updateUI(user);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("Auth", "signInWithEmail:failure", task.getException());
                                Toast.makeText(Login.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                updateUI(null);
                            }

                        }
                    });
        }
    }
    private void updateUI(FirebaseUser user) {
        if(user == null){
            Toast.makeText(this, "The username or password your entered is incorrect", Toast.LENGTH_SHORT).show();
        }
        else{
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
    private boolean variableData() {
        String textemail = (email.getText().toString()).trim();
        String Password = (password.getText().toString()).trim();
        if(!isValidEmailAddress(textemail)){
            Toast.makeText(Login.this, "The email invalid", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(Password.length()<6){
            Toast.makeText(Login.this, "The Password must be at least 6 characters", Toast.LENGTH_SHORT).show();
            return false;
        }
            return true;
    }
    private void bntsign_up() {
        startActivityForResult(new Intent(this, Register.class),999);
    }

    private void bntagreement() {

    }
    public static boolean isValidEmailAddress(String email) {
        boolean stricterFilter = true;
        String stricterFilterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        String laxString = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
        String emailRegex = stricterFilter ? stricterFilterString : laxString;
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(emailRegex);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}
