package tlive.techatrium.com.lsa.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tlive.techatrium.com.lsa.R;

/**
 * Created by hongt on 01/11/2017.
 */

public class EmptyFragment extends Fragment {
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view==null){
            view = new View(getActivity());
        }
        return view;
    }
}