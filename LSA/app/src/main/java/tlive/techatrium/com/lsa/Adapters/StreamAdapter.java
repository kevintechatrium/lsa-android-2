package tlive.techatrium.com.lsa.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import tlive.techatrium.com.lsa.Models.StreamModel;
import tlive.techatrium.com.lsa.R;

/**
 * Created by hongt on 06/11/2017.
 */

public class StreamAdapter extends ArrayAdapter<StreamModel> {
    Context mC;
    ArrayList<StreamModel> mL = new ArrayList<StreamModel>();

    public StreamAdapter(Context context, int resource, List<StreamModel> objects) {
        super(context, resource, objects);
        this.mC = context;
        this.mL = new ArrayList<StreamModel>(objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowview = convertView;
        viewItem hv = null;
        if (rowview == null) {
            LayoutInflater ift = (LayoutInflater) mC.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = ift.inflate(R.layout.stream_item, null);
            hv = new viewItem();
            hv.title = (TextView) rowview.findViewById(R.id.title);
            hv.name = (TextView) rowview.findViewById(R.id.displayName);
            hv.avatar = (CircleImageView) rowview.findViewById(R.id.userAvatar);
            hv.thumb = (ImageView) rowview.findViewById(R.id.thumb);
            hv.currentView = (TextView) rowview.findViewById(R.id.currentViewers);
            rowview.setTag(hv);
        } else {
            hv = (viewItem) convertView.getTag();
        }
        StreamModel streamModel = mL.get(position);

        hv.title.setText(streamModel.getTitle());
        hv.name.setText(streamModel.getOwner().getFullName());
        hv.currentView.setText(streamModel.getCurrentViewers() + "");
        if (streamModel.getOwner().getAvatar().length() > 1)
            Picasso.with(mC).load(streamModel.getOwner().getAvatar()).into(hv.avatar);
        if (streamModel.getOwner().getCoverImage().length() > 1)
            Picasso.with(mC).load(streamModel.getOwner().getCoverImage()).into(hv.thumb);
        return rowview;

    }

    static class viewItem {
        TextView title, name, currentView;
        CircleImageView avatar;
        ImageView thumb;
    }

    public void Wrap_Items(ArrayList<StreamModel> items) {
        this.mL = items;
        notifyDataSetChanged();
    }
}
