package tlive.techatrium.com.lsa.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.lake.librestreaming.filter.softvideofilter.BaseSoftVideoFilter;
import me.lake.librestreaming.model.RESConfig;
import tlive.techatrium.com.lsa.Fragment.EmptyFragment;
import tlive.techatrium.com.lsa.Fragment.StreamFragment;
import tlive.techatrium.com.lsa.R;
import tlive.techatrium.com.lsa.softfilter.SkinBlurFilterSoft;

/**
 * Created by lake on 16-5-31.
 */
public class SoftStreamingActivity extends BaseStreamingActivity {
    private StreamFragment streamFragment;
    private EmptyFragment emptyFragment;
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        filtermode = RESConfig.FilterMode.SOFT;
        SkinBlurFilterSoft soft;
        super.onCreate(savedInstanceState);
        /**
         * filters just for demo
         */
        soft = new SkinBlurFilterSoft(this);
        resClient.setSoftVideoFilter(soft);
    }

    @Override
    public void onStartStream() {
        if (adapter == null) {
            adapter = new ViewPagerAdapter(getSupportFragmentManager());
        }
        pager.setAdapter(adapter);
        pager.setCurrentItem(1);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        public Fragment getItem(int num) {
            switch (num) {
                case 0:
                    return new EmptyFragment();
                case 1:
                    if (streamFragment == null) {
                        streamFragment = StreamFragment.getInstance(mDatabase, resClient, currentStreamID,currentUser);
                    }
                    return streamFragment;

            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Sezione " + position;
        }

    }
}
