package tlive.techatrium.com.lsa.Fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import tlive.techatrium.com.lsa.Activities.LiveVideoPlayerActivity;
import tlive.techatrium.com.lsa.Adapters.StreamAdapter;
import tlive.techatrium.com.lsa.Models.StreamModel;
import tlive.techatrium.com.lsa.R;

/**
 * Created by hongt on 01/11/2017.
 */

@SuppressLint("ValidFragment")
public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ListView mListview;
    private Context context;
    private static String TAG = "HomeFragmentDebug";
    private ArrayList<StreamModel> listStream = new ArrayList<>();
    private int maxDisplay = 10;
    private StreamModel stream;
    private Query query;
    private StreamAdapter adapter;
    private boolean refreshing = false;
    private boolean WaitAnimation = false;
    private SwipeRefreshLayout refreshLayout;
    private View view;
    private static HomeFragment homeFragment;
    @SuppressLint("ValidFragment")
    public HomeFragment(DatabaseReference mDatabase, FirebaseAuth mAuth) {
        this.mDatabase = mDatabase;
        this.mAuth = mAuth;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static HomeFragment getInstance(DatabaseReference mDatabase, FirebaseAuth mAuth) {
        homeFragment = homeFragment == null ? new HomeFragment(mDatabase, mAuth) : homeFragment;
        return homeFragment;
    }

    private void initData() {
        context = getActivity();
        adapter = new StreamAdapter(context, R.layout.stream_item, listStream);
        mListview.setAdapter(adapter);
        query = mDatabase.child("stream_live_room").limitToLast(15).orderByChild("status").equalTo("live");
        updateData();
    }

    private void updateData() {
        listStream.clear();

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        stream = data.getValue(StreamModel.class);
                        listStream.add(0, stream);
                        initDisplay();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onDataChange: ", e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateRefresh() {
        if (refreshing) {
            refreshLayout.setRefreshing(false);
            refreshing = false;
        }
    }

    private void initEvent() {
        refreshLayout.setOnRefreshListener(this);
        mListview.setOnItemClickListener(this);
    }

    private void initDisplay() {
        updateRefresh();
        adapter.Wrap_Items(listStream);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.home_fragment, container, false);
            initControl(view);
            initEvent();
            initData();
        }
        return view;
    }

    private void initControl(View view) {
        mListview = (ListView) view.findViewById(R.id.list);
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
    }

    @Override
    public void onRefresh() {
        refreshing = true;
        updateData();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(context, LiveVideoPlayerActivity.class);
        intent.putExtra("path", listStream.get(position).getKey());
        startActivity(intent);
    }
}